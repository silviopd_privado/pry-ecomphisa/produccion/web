/**
 * Theme: Highdmin - Responsive Bootstrap 4 Admin Dashboard
 * Author: Coderthemes
 * Form Pickers
 */
jQuery(document).ready(function() {
    // Date Picker
    jQuery('#datepicker-autoclose').datepicker({
        locale: 'es',
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    jQuery('#datepicker-autoclose2').datepicker({
        locale: 'es',
        format: 'dd/mm/yyyy',
        autoclose: true
    });

    $("#datepicker-autoclose").datepicker().datepicker("setDate", new Date());
    $("#datepicker-autoclose2").datepicker().datepicker("setDate", new Date());

});
